export default function isHidden(el) {
	// Note: does not work on position: fixed elements! - use getComputedStyle().visibility (which is more expensive)
	return el.offsetParent === null;
}
