export default function getAnchorTarget(link) {
	var id = link.hash.replace("#", "");
	return document.getElementById(id) || null;
}
