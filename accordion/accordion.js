export class Accordion {
    constructor(container) {
        this.container = container;
        this.init(true);

        window.addEventListener('resize', _ => {
            this.attachHandlers(false);
        })
    }

    init(setupHandlers) {
        let $triggers = this.container.querySelectorAll("[data-accordion-trigger]");
        $triggers.forEach($t => {
            const contentId = $t.getAttribute("aria-controls");
            const $content = $acc.querySelector(`#${contentId}`);
            this.setAccordionHeights($content);
            if (setupHandlers) {
                this.clickHandlers($t, $content);
            }
        })
    }

    setAccordionHeights($content) {
        const opened = $content.parentNode.classList.contains('is-active');
        $content.style.setProperty("--accordion-height", "auto");

        $content.classList.remove('is-closed');

        $content.style.setProperty(
            "--acordion-height",
            $content.offsetHeight + 'px'
        );

        if (!opened) {
            $content.classList.add("is-closed");
        }
    }

    clickHandler($trigger, $content) {
        $trigger.addEventListener("click", e => {
            const ariaHiddenValue = attrBool($content.getAttribute("aria-hidden"));
            const ariaExpandedValue = attrBool($trigger.getAttribute("aria-expanded"));

            $trigger.parentNode.classList.toggle("is-active");
            $trigger.setAttribute("aria-expanded", !ariaExpandedValue);
            $content.setAttribute("aria-hidden", !ariaHiddenValue);
            $content.classList.toggle("is-closed");
            e.preventDefault();
        });
    }

    attrBool(attrValue) {
        return attrValue === "true" ? true : false;
    }
}