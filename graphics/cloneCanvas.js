export default function cloneCanvas(sourceCanvas) {
	var destinationCanvas = document.createElement("canvas");
	destinationCanvas.width = sourceCanvas.width;
	destinationCanvas.height = sourceCanvas.height;

	var destCtx = destinationCanvas.getContext("2d");

	destCtx.drawImage(sourceCanvas, 0, 0);

	return destinationCanvas;
}
