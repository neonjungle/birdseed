function getUrl(relativePath) {
	if (relativePath) {
		if (relativePath.charAt(0) === "/") {
			relativePath = relativePath.slice(1);
		}
		return window.location.origin + window.location.pathname + relativePath;
	} else {
		return window.location.origin + window.location.pathname;
	}
}
