export default function addQueryParam(url, param, value) {
	param = encodeURIComponent(param);
	var a = document.createElement("a");
	a.href = url;
	var regex = new RegExp("(\\?|&amp;|&)(" + param + ")(=([^&]*))*");
	var str = param + (value ? "=" + encodeURIComponent(value) : "");
	var q = a.search.replace(regex, "$1" + str);
	if (q === a.search) {
		a.search += (a.search ? "&" : "") + str;
	}
	return a.href;
}
