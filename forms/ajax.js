/**
 * This class takes three elements, the form used for the requests, the target element to replace with the server response and the element to output server errors to.
 * If the target is ommitted it's assumed the form itself is to be replaced. If the error element is ommitted then one is looked for in the form using the data-ajax-error attribute.
 * Some assumptions are made about the data returned from the server, but we control this so it should be predictable.
 */
export class AjaxForm {
	constructor(form, target, errorElement) {
		this.form = form;
		this.target = target ? target : form;
		this.errorElement = errorElement
			? errorElement
			: form.querySelector("[data-ajax-error]");
		this.submitButton = this.form.querySelector(
			'input[type="submit"], button[type="submit"]'
		);
		this.setupListeners();
	}

	setupListeners() {
		this.form.addEventListener("submit", e => {
			e.preventDefault();
			this.fetchAndReplace();
		});
	}

	getFetchOptions(formMethod) {
		return {
			method: formMethod,
			credentials: 'same-origin',
			headers: {
				"X-Requested-With": "XMLHttpRequest"
			},
		}
	}

	fetchAndReplace() {
		this.beforeFetch();
		this.toggleSubmitButtonState();

		let formMethod = this.form.method ? this.form.method.toUpperCase() : "POST";
		let fetchOptions = this.getFetchOptions(formMethod);
		let formAction = this.form.action;
		const formData = new FormData(this.form)

		if (formMethod == "POST") {
			fetchOptions['body'] = formData
		} else if (formMethod == "GET") {
			const parameters = Array.from(formData.entries()).map(
				([key, val]) => `${key}=${encodeURIComponent(val)}`
			).join('&')
			formAction = `${formAction}?${parameters}`;
		} else {
			throw Error("Unknown form method type: " + formMethod)
		}

		fetch(formAction, fetchOptions)
			.then(response => response.json())
			.then(responseJson => {
				if (responseJson.success) {
					this.replaceTarget(responseJson.payload);
				} else {
					this.handleError(responseJson.message);
				}
				this.toggleSubmitButtonState();
				this.afterFetch(responseJson);
			})
			.catch(e => {
				this.handleError(
					"Sorry there was an error, an administrator has been notified. Refresh the page and try again."
				);
				// We rethrow the errror so it can be picked up by bugsnag
				throw e;
			});
	}

	beforeFetch() {
		this.target.classList.add("is-loading");
	}

	afterFetch(responseJson) {
		this.target.classList.remove("is-loading");
	}

	toggleSubmitButtonState() {
		if (this.submitButton) {
			this.submitButton.disabled = !this.submitButton.disabled;
		}
	}

	replaceTarget(html) {
		this.target.innerHTML = html;
	}

	handleError(message) {
		if (!this.errorElement) return;
		this.errorElement.innerText = message;
	}
}
