export { multipleCheckboxRequiredValitate, basicFieldValitate };

const basicFieldValitate = {
	isApplicable: function() {
		return true;
	},
	validate: function($field, $fieldContainer) {
		if ($field.validity.valid) {
			return [];
		}

		const errors = [];

		if ($field.validity.valueMissing) {
			errors.push("This field is required");
		}

		if ($field.validity.patternMismatch || $field.validity.typeMismatch) {
			if ($field.dataset.errorMessage) {
				errors.push($field.dataset.formsErrorMessage);
			} else if ($field.type === "email") {
				errors.push("This field must be a valid email address");
			} else if ($field.type === "number") {
				errors.push("This field must be a number");
			}
		}

		return errors;
	}
};

const multipleCheckboxRequiredValitate = {
	isApplicable: function($field, $fieldContainer) {
		return (
			$fieldContainer.dataset.required !== undefined &&
			$field.type === "checkbox"
		);
	},
	validate: function($field, $fieldContainer) {
		const hasSelectedCheckbox =
			Array.from($fieldContainer.querySelectorAll(this.fieldSelector)).filter(
				$f => $f.checked
			).length > 0;

		return hasSelectedCheckbox
			? []
			: ["At least one checkbox must be selected"];
	}
};
