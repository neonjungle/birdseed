import {
	multipleCheckboxRequiredValitate,
	basicFieldValitate
} from "./validators";

export class ValidatedForm {
	/**
	 * Create a new validated form
	 *
	 * @param {HTMLElement} formElement The base form DOM element to apply validation to.
	 * @param {String} fieldContainerSelector A CSS selector string which is used to find the field containers
	 * @param {String} fieldSelector A CSS selector string which is used to find fields within field containers to validate
	 * @param {String} errorClassPrefix The string to add to the start of the error element classes
	 * within formElement.
	 */
	constructor(
		formElement,
		fieldContainerSelector,
		fieldSelector,
		errorClassPrefix
	) {
		if (!formElement) {
			console.error(
				`ValidatedForm requires a formElement. Passed formElement was: ${formElement}`
			);
		}

		this.formElement = formElement;
		this.fieldContainerSelector = fieldContainerSelector || ".form-field";
		this.fieldSelector =
			fieldSelector ||
			"input:not([data-novalidate]), select:not([data-novalidate]), textarea:not([data-novalidate])";
		this.errorClassPrefix = errorClassPrefix || "form-field";
		this.valid = true;
		this.formFieldContainers = this.formElement.querySelectorAll(
			this.fieldContainerSelector
		);

		this.validators = [];

		this.setupValidators();
		this.attachValidationEvents();
	}

	/**
	 * Add a custom validator to the Form. Allows completely custom form validation. Both parameter
	 * functions are passed an individual field and the field component container($field, $fieldContainer).
	 *
	 * @param {Function} applicableFn Function which returns true if validateFn can be used to validate
	 * a form control.
	 * @param {Function} validateFn Function which returns true if the passed control is valid or an
	 * array of error messages if it's not.
	 */
	addValidator(applicableFn, validateFn) {
		this.validators.push({
			isApplicable: applicableFn,
			validate: validateFn
		});
	}

	/**
	 * Functions past this point can be called externally or overridden, but require an understanding
	 * of the class to do so.
	 */
	setupValidators() {
		this.validators.push(basicFieldValitate);
		this.validators.push(multipleCheckboxRequiredValitate);
	}

	attachValidationEvents() {
		this.formElement.addEventListener("submit", e =>
			this.runValidation.call(this, this.formFieldContainers, e)
		);

		this.formFieldContainers.forEach($fieldContainer => {
			$fieldContainer.querySelectorAll(this.fieldSelector).forEach($field => {
				$field.addEventListener("blur", _ =>
					this.runValidation.call(this, [$fieldContainer])
				);
			});
		});
	}

	runValidation(fieldContainers, event = false) {
		this.valid = true;

		fieldContainers.forEach($fieldContainer => {
			this.validators.forEach(validator =>
				this.runValidator($fieldContainer, validator)
			);
		});

		if (!this.valid && event) {
			event.preventDefault();
		}
	}

	runValidator($fieldContainer, validator) {
		let $field = $fieldContainer.querySelector(this.fieldSelector);
		if (validator.isApplicable.call(this, $field, $fieldContainer)) {
			let errors = validator.validate.call(this, $field, $fieldContainer);

			if (errors.length > 0) {
				this.valid = false;
			}

			this.updateField($fieldContainer, errors);
		}
	}

	updateField($fieldContainer, errors) {
		const $errorMessagesContainer = $fieldContainer.querySelector(
			`.${this.errorClassPrefix}--error-messages`
		);

		if (errors.length < 1) {
			$errorMessagesContainer.innerHTML = "";
			$fieldContainer.classList.remove("is-invalid");
			$fieldContainer.classList.add("is-valid");
		} else {
			$errorMessagesContainer.innerHTML = errors.map(
				error =>
					`<p class="${
					this.errorClassPrefix
					}--error-messages-single">${error}</p>`
			);
			$fieldContainer.classList.add("is-invalid");
			$fieldContainer.classList.remove("is-valid");
		}
	}
}
