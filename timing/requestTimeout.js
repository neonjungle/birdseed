/*
	The same as window.setTimeout() except it forces the callback to be called
	in the next available frame after the timeout. This helps avoid calling a function during or
	towards the end of a frame when there may not be enough time in the 16ms frame budget left
	for us to do our calculations before ushing that frame out longer than 16ms which would then
	drop a frame causing visual jank.

	@function requestTimeout
	@param {function} fn - Function to call after the delay
	@param {number} delay - Delay in ms to wait before invoking fn
	@returns {number} The request id so that other browser APIs can interact with that request (eg window.cancelAnimationFrame)
*/
export default function requestTimeout(fn, delay) {
	let start = new Date().getTime();
	let rafID;

	function loop() {
		let current = new Date().getTime();
		let delta = current - start;

		delta >= delay ? fn.call() : (rafID = requestAnimationFrame(loop));
	}

	rafID = requestAnimationFrame(loop);
	return rafID;
}
