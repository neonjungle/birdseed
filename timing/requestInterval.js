/*
	requestInterval is the same as window.setInterval except it forces the callback to be called
	in the frame after the previous callback. This helps avoid overloading frames and causing them to
	drop thus reducing the fps.

	@function requestInterval
	@param {function} fn - Function to call each interval
	@param {number} delay - Delay in ms to wait before invoking fn on each iteration
	@returns {number} The request id so that other browser APIs can interact with that request (eg window.cancelAnimationFrame)
*/
export default function requestInterval(fn, delay) {
	let start = new Date().getTime();
	let rafID; // raf = requestAnimationFrame

	function loop() {
		let current = new Date().getTime();
		let delta = current - start;

		if (delta >= delay) {
			fn.call();
			start = new Date().getTime();
		}

		rafID = requestAnimationFrame(loop);
	}

	rafID = requestAnimationFrame(loop);

	return rafID;
}
