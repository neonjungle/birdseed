import Bowser from "bowser";

export class OutdatedBrowserNotice {
	constructor(browserVersions, outdatedCallback) {
		// Note: Browsers not listed here count as *unsupported* browsers
		this.browserVersions = browserVersions || {
			ie: ">10",
			edge: ">=16",
			safari: ">=10",
			opera: ">=55",
			chrome: ">=70",
			firefox: ">=60"
		};
		this.outdatedCallback = outdatedCallback;
		this.browser = Bowser.getParser(window.navigator.userAgent);

		this.applyPollyfills();
		this.test();
	}

	test() {
		if (this.isValidBrowser()) {
			this.outdatedCallback(false);
		} else {
			let browserDetails = this.browser.getBrowser();
			let versionString = "";

			// Unfortunately we dont always get the version (see isValidBrowser notes)
			if (browserDetails.version) {
				versionString = `${browserDetails.name} version ${
					browserDetails.version
				}`;
			} else {
				versionString = `an out of date version of ${browserDetails.name}`;
			}

			document.body.insertAdjacentHTML(
				"afterbegin",
				this.noticeTemplate(versionString)
			);
			this.outdatedCallback(true);
		}
	}

	noticeTemplate(versionString) {
		return `
			<div data-notice class='browser-detect'>
				<div class='browser-detect--notice richtext'>
					<h2>Outdated browser</h2>
					<p>You are using ${versionString}. For a better browsing experience, we recommend that you update.</p>
				</div>
				<div class='browser-detect--actions'>
					<a class='browser-detect--button' href='http://outdatedbrowser.com/en'>Update</a>
				</div>
			</div>
		`;
	}

	isValidBrowser() {
		const fullBrowserDetails = this.browser.getResult();
		// Work around Bowser not giving us a browser version to work with when in WebView on iOS. -JS
		// See this issue for more details: https://github.com/lancedikson/bowser/issues/204
		if (
			fullBrowserDetails.os.name === "iOS" &&
			!fullBrowserDetails.browser.version
		) {
			let majorVersion = parseInt(fullBrowserDetails.os.version.split(".")[0]);
			return majorVersion >= 10;
		}
		return this.browser.satisfies(this.browserVersions);
	}

	applyPollyfills() {
		// For older versions of Safari (prior to v10)
		// From https://gist.github.com/eligrey/1276030
		if (
			document &&
			!(
				"insertAdjacentHTML" in
				document.createElementNS("http://www.w3.org/1999/xhtml", "_")
			)
		) {
			HTMLElement.prototype.insertAdjacentHTML = function(position, html) {
				"use strict";

				var ref = this;
				var container = ref.ownerDocument.createElementNS(
					"http://www.w3.org/1999/xhtml",
					"_"
				);
				var ref_parent = ref.parentNode;
				var node, first_child, next_sibling;

				container.innerHTML = html;

				switch (position.toLowerCase()) {
					case "beforebegin":
						while ((node = container.firstChild)) {
							ref_parent.insertBefore(node, ref);
						}
						break;
					case "afterbegin":
						first_child = ref.firstChild;
						while ((node = container.lastChild)) {
							first_child = ref.insertBefore(node, first_child);
						}
						break;
					case "beforeend":
						while ((node = container.firstChild)) {
							ref.appendChild(node);
						}
						break;
					case "afterend":
						next_sibling = ref.nextSibling;
						while ((node = container.lastChild)) {
							next_sibling = ref_parent.insertBefore(node, next_sibling);
						}
						break;
				}
			};
		}
	}
}
