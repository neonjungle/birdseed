export class StripeForm {
	/**
	 * Create credit card inputs for stripe and the handlers for creating payment tokens.
	 *
	 * @param {HTMLElement} form The base form DOM element to add the fields.
	 * @param {String} stripePublishableKey Public api key for communicating with stripe.
	 * @param {String} fieldStyles An optional object to define the styles for the fields. See: https://stripe.com/docs/stripe-js/reference#element-options
	 */
	constructor(form, stripePublishableKey, fieldStyles) {
		if (!Stripe) {
			console.error(
				"Stripe JS not found, see https://stripe.com/docs/stripe-js/elements/quickstart#setup"
			);
		}
		if (!stripePublishableKey) {
			console.error("Stripe publishable key missing");
		}
		this.form = form;
		this.stripe = Stripe(stripePublishableKey);
		this.fieldStyles = fieldStyles || {
			base: {
				fontSize: "16px"
			},
			empty: {
				color: "#0c0c0c"
			},
			invalid: {
				color: "red"
			}
		};
		this.renderFields();
		this.attachHandlers();
		this.paymentOk = false;
	}

	renderFields() {
		let container = this.form.querySelector(".card-elements");
		const fieldClass = this.formFieldClass;
		this.fieldProperties.forEach(fieldProps => {
			container.innerHTML += `
 			<div class="${fieldClass} ${fieldClass}__${fieldProps.id}" no-validate>
 				<label for="${fieldProps.id}" class="${fieldClass}--label">
 					${fieldProps.label}
 				</label>
 				<div id="${fieldProps.id}"></div>
 				<div class="${fieldClass}--error-messages"></div>
 			</div>`;
		});
		container.innerHTML += `<div id='card-errors' class='${fieldClass}--error-messages'></div>`;
	}

	attachHandlers() {
		const elements = this.stripe.elements();
		const options = {
			style: this.fieldStyles,
			classes: this.stripeClasses
		};
		const cardElements = this.fieldProperties.map(fieldProps => {
			let cardElement = elements.create(fieldProps.element, options);
			cardElement.mount(`#${fieldProps.id}`);
			cardElement.on("change", event => {
				if (event.error) {
					this.handleError(event.error.message);
				} else {
					this.clearErrors();
				}
			});
			return cardElement;
		});
		this.form.addEventListener("submit", e => {
			if (!this.paymentOk) {
				e.preventDefault();
				const submitButton = this.form.querySelector("button[type='submit']");
				this.requestToken(submitButton, cardElements[0]);
			}
		});
	}

	requestToken(submitButton, cardElement) {
		submitButton.disabled = true;
		submitButton.classList.add("is-waiting");
		this.stripe.createToken(cardElement).then(result => {
			submitButton.classList.remove("is-waiting");
			if (result.error) {
				this.handleError(result.error.message);
				submitButton.disabled = false;
			} else {
				this.insertToken(result.token);
				this.paymentOk = true;
				this.form.submit();
			}
		});
	}

	handleError(message) {
		const errorContainer = this.form.querySelector("#card-errors");
		errorContainer.innerHTML = `<p class='${
			this.formFieldClass
		}--messages-single'>${message}<p>`;
	}

	clearErrors() {
		const errorContainer = this.form.querySelector("#card-errors");
		errorContainer.innerHTML = "";
	}

	insertToken(token) {
		let hiddenInput = this.form.querySelector(`input[name="stripeToken"]`);
		if (hiddenInput) {
			hiddenInput.setAttribute("value", token.id);
		} else {
			hiddenInput = document.createElement("input");
			hiddenInput.setAttribute("type", "hidden");
			hiddenInput.setAttribute("name", "stripeToken");
			hiddenInput.setAttribute("value", token.id);
			this.form.appendChild(hiddenInput);
		}
	}

	get stripeClasses() {
		return {
			base: `${this.formFieldClass}--field`,
			focus: "focus",
			empty: "empty",
			invalid: "is-invalid"
		};
	}

	get fieldProperties() {
		return [
			{
				id: "card-number",
				label: "Credit card number",
				element: "cardNumber"
			},
			{
				id: "card-expiry",
				label: "Expiry Date",
				element: "cardExpiry"
			},
			{
				id: "card-cvc",
				label: "CVC",
				element: "cardCvc"
			}
		];
	}

	get formFieldClass() {
		return "form-field";
	}
}
